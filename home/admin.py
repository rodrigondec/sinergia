from django.contrib import admin

from .models import Contador, Introducao, Sobre, Programacao, Palestrantes, Perguntas, Parceiros, Rolou, RolouFotos


admin.site.register(Contador)
admin.site.register(Introducao)
admin.site.register(Sobre)
admin.site.register(Programacao)
class PalestrantesAdmin(admin.ModelAdmin):
  list_display = ['nome', 'slug', 'imagem']
  search_fields = ['nome', 'slug']
  prepopulated_fields = {'slug': ['nome']}
admin.site.register(Palestrantes, PalestrantesAdmin)
admin.site.register(Perguntas)
admin.site.register(Parceiros)
admin.site.register(Rolou)
admin.site.register(RolouFotos)
