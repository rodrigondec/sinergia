from django import forms
from django.core.mail import send_mail
from django.conf import settings



#from django.core.mail import EmailMultiAlternatives
#from django.template.loader import get_template





class Contato(forms.Form):

  nome = forms.CharField(max_length=100, widget = forms.TextInput(attrs={'placeholder':'Nome', 'required':''}))
  email = forms.CharField(max_length=150, widget = forms.TextInput(attrs={'placeholder':'E-mail', 'required':'', 'type':'email'}))
  assunto = forms.CharField(max_length=100, widget = forms.TextInput(attrs={'placeholder':'Assunto', 'required':''}))
  mensagem = forms.CharField(max_length=250, widget = forms.TextInput(attrs={'placeholder':'Mensagem', 'required':''}))
  url = forms.CharField(max_length=250, widget = forms.HiddenInput())

  def send_mail(self):
    mensagem = '--------------------------------------------------------------------------------\n'
    mensagem += ' Novo email de contato no site Forum Sinergia \n'
    mensagem += '--------------------------------------------------------------------------------\n'
    mensagem += 'Nome: %(nome)s \n'
    mensagem += 'E-mail: %(email)s \n'
    mensagem += 'Assunto: %(assunto)s \n'
    mensagem += 'Mensagem: %(mensagem)s \n'


    context = {
      'nome': self.cleaned_data['nome'],
      'email': self.cleaned_data['email'],
      'assunto': self.cleaned_data['assunto'],
      'mensagem': self.cleaned_data['mensagem'],
      'urlmail': self.cleaned_data['url'],
    }

    '''template = get_template('email.html')
    content = template.render(context)


    text_content = mensagem % context


    html_content = content
    msg = EmailMultiAlternatives('[Forum Sinergia] Contato do Site', text_content, settings.DEFAULT_FROM_EMAIL, [settings.CONTACT_EMAIL])
    msg.attach_alternative(html_content, "text/html")
    msg.send()'''

    mensagem = mensagem % context
    send_mail('[Forum Sinergia] Contato do Site', mensagem, settings.DEFAULT_FROM_EMAIL, [settings.CONTACT_EMAIL])
