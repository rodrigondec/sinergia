# Generated by Django 2.1 on 2018-08-19 22:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0006_auto_20180819_1934'),
    ]

    operations = [
        migrations.AddField(
            model_name='palestrantes',
            name='imagem',
            field=models.ImageField(blank=True, upload_to='imagens', verbose_name='Imagem'),
        ),
        migrations.AddField(
            model_name='parceiros',
            name='imagem',
            field=models.ImageField(blank=True, upload_to='imagens', verbose_name='Imagem'),
        ),
        migrations.AddField(
            model_name='sobre',
            name='imagem',
            field=models.ImageField(blank=True, upload_to='imagens', verbose_name='Imagem'),
        ),
    ]
