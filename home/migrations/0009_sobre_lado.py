# Generated by Django 2.1 on 2018-08-19 23:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0008_parceiros_nome'),
    ]

    operations = [
        migrations.AddField(
            model_name='sobre',
            name='lado',
            field=models.CharField(choices=[('21466c', 'Azul'), ('e9b53d', 'Amarelo'), ('e21c3b', 'Vermelho')], default='direction-r', max_length=6, verbose_name='Lado do Texto'),
        ),
    ]
