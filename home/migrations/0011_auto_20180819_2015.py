# Generated by Django 2.1 on 2018-08-19 23:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0010_auto_20180819_2015'),
    ]

    operations = [
        migrations.AlterField(
            model_name='programacao',
            name='lado',
            field=models.CharField(choices=[('direction-r', 'Direito'), ('direction-l', 'Esquerdo')], default='direction-r', max_length=6, verbose_name='Lado do Texto'),
        ),
    ]
