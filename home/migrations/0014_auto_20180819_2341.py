# Generated by Django 2.1 on 2018-08-20 02:41

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0013_contador_data'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='contador',
            options={'verbose_name': 'Contador', 'verbose_name_plural': 'Contador'},
        ),
    ]
