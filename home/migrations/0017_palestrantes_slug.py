# Generated by Django 2.1 on 2018-08-20 03:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0016_auto_20180819_2342'),
    ]

    operations = [
        migrations.AddField(
            model_name='palestrantes',
            name='slug',
            field=models.SlugField(default='none', verbose_name='Atalho'),
        ),
    ]
