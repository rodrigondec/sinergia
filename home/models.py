from django.db import models
from datetime import date

class Contador(models.Model):
  data = models.DateField('Data', default=date.today)
  

  class Meta:
    verbose_name = 'Contador'
    verbose_name_plural = 'Contador'


CORES=[ ('21466c','Azul'), 
        ('e9b53d','Amarelo'), 
        ('e21c3b','Vermelho')]
class Introducao(models.Model):
  icone = models.ImageField(upload_to='imagens', verbose_name="Icone", null=False, blank=True)
  cor = models.CharField('Cor do Icone',max_length=6, choices=CORES, null=False, default="21466c")
  titulo = models.CharField('Título', max_length=150)
  texto = models.TextField('Texto Descrição', null=False, blank=True)

  def __str__(self):
    return self.titulo

  class Meta:
    verbose_name = 'Introdução'
    verbose_name_plural = 'Introduções'


class Sobre(models.Model):
  imagem = models.ImageField(upload_to='imagens', verbose_name="Imagem", null=False, blank=True)
  texto = models.TextField('Texto Descrição', null=False, blank=True)
  link = models.CharField('Link - Com "http://"', max_length=150, blank=True)

  class Meta:
    verbose_name = 'Sobre'
    verbose_name_plural = 'Sobre'

LADO=[  ('direction-r','Direito'), 
        ('direction-l','Esquerdo')]
class Programacao(models.Model):
  balao = models.CharField('Texto Balão', max_length=150)
  texto = models.TextField('Texto Descrição', null=False, blank=True)
  lado = models.CharField('Lado do Texto',max_length=11, choices=LADO, null=False, default="direction-r")

  def __str__(self):
    return self.balao

  class Meta:
    verbose_name = 'Programação'
    verbose_name_plural = 'Programação'


class Palestrantes(models.Model):
  imagem = models.ImageField(upload_to='imagens', verbose_name="Imagem", null=False, blank=True)
  nome = models.CharField('Nome', max_length=150)
  profissao = models.CharField('Profissão', max_length=150)
  dia = models.CharField('Dia da Palestra', max_length=150)
  texto = models.TextField('Texto Descrição', null=False, blank=True)
  slug = models.SlugField('Atalho')
  prepopulated_fields = {'slug': ['nome']}

  def __str__(self):
    return self.nome

  class Meta:
    verbose_name = 'Palestrante'
    verbose_name_plural = 'Palestrantes'


class Perguntas(models.Model):
  pergunta = models.CharField('Pergunta', max_length=250)
  resposta = models.TextField('Resposta', null=False, blank=True)

  def __str__(self):
    return self.pergunta

  class Meta:
    verbose_name = 'Pergunta'
    verbose_name_plural = 'Perguntas'


PARCERIA=[ ('co','Co-realizador'), 
        ('par','Parceiro'), 
        ('apo','Apoiador')]
class Parceiros(models.Model):
  imagem = models.ImageField(upload_to='imagens', verbose_name="Imagem", null=False, blank=True)
  nome = models.CharField('Nome', max_length=150, default=" ")
  link = models.CharField('Link - Com "http://"', max_length=150)
  tipo = models.CharField('Tipo de Parceria', max_length=15, choices=PARCERIA, null=False, default="co")

  def __str__(self):
    return self.nome

  class Meta:
    verbose_name = 'Parceiro'
    verbose_name_plural = 'Parceiros'

class Rolou(models.Model):
  descricao = models.TextField('Descrição', null=False, blank=True)
  link = models.CharField('Vídeo Link - Com "http://"', max_length=150)

  class Meta:
    verbose_name = 'Rolou - Principal'
    verbose_name_plural = 'Rolou - Principal'


class RolouFotos(models.Model):
  imagem = models.ImageField(upload_to='imagens', verbose_name="Imagem", null=False, blank=True)
  descricao = models.CharField('Descrição', max_length=150, default=" ")

  def __str__(self):
    return self.descricao

  class Meta:
    verbose_name = 'Rolou - Fotos'
    verbose_name_plural = 'Rolou - Fotos'