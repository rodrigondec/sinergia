from django.shortcuts import render
from home.models import Contador, Introducao, Sobre, Programacao, Palestrantes, Perguntas, Parceiros, Rolou, RolouFotos

from .forms import Contato

def index(request):

  contador = Contador.objects.all()[0]
  intro = Introducao.objects.all()
  sobre = Sobre.objects.all()[0]
  programacao = Programacao.objects.all()
  palestrantes = Palestrantes.objects.all()
  perguntas = Perguntas.objects.all()
  parceiros = Parceiros.objects.all()
  jarolou = Rolou.objects.all()[0]
  jaroloufotos = RolouFotos.objects.all()

  jarolou.link = jarolou.link.split('=')[1]

  context = {
    "contador": contador,
    "intros": intro, 
    "sobre": sobre,
    "programacao": programacao,
    "palestrantes": palestrantes,
    "perguntas": perguntas,
    "parceiros": parceiros,
    "jarolou": jarolou,
    "jaroloufotos": jaroloufotos
  }

  if request.method == 'POST': 
    form = Contato(request.POST)
    if form.is_valid():
      context['is_valid'] = True
      form.send_mail()
      form = Contato() 
  else:
    form = Contato()
  context['form'] = form

  return render(request, 'index.html', context)


def palestrante(req, slug):

  palestrante = Palestrantes.objects.get(slug=slug)

  context = {
    'palestrante': palestrante,
  }

  if req.method == 'POST': 
    form = Contato(req.POST)
    if form.is_valid():
      context['is_valid'] = True
      form.send_mail()
      form = Contato() 
  else:
    form = Contato()
  context['form'] = form

  
  return render(req, 'palestrante.html', context)

